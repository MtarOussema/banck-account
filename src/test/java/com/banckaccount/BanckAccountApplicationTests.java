package com.banckaccount;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTestContextBootstrapper;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.banckaccount.exceptions.CompteBancaireNonTrouveException;
import com.banckaccount.model.CompteBancaire;
import com.banckaccount.model.Transaction;
import com.banckaccount.repository.CompteBancaireRepository;
import com.banckaccount.repository.TransactionRepository;
import com.banckaccount.service.imp.CompteBancaireServiceImpl;
import com.banckaccount.service.imp.TransactionServiceImpl;
@RunWith(SpringRunner.class)
@SpringBootTest
@BootstrapWith(SpringBootTestContextBootstrapper.class)
class BanckAccountApplicationTests {
	
	  @Mock
	    private CompteBancaireRepository compteBancaireRepository;

	    @Mock
	    private TransactionRepository transactionRepository;

	    @InjectMocks
	    private CompteBancaireServiceImpl compteBancaireService;
	    
	    @InjectMocks
	    private TransactionServiceImpl transactionServiceImpl ;

	    @BeforeEach
	    void setUp() {
	        MockitoAnnotations.openMocks(this);
	    }

	    @Test
	    void testDeposerMontant() {
	        // Données de test
	        String numeroCompte = "123456";
	        double montant = 100.0;

	        // Compte bancaire existant
	        CompteBancaire compteBancaire = new CompteBancaire();
	        compteBancaire.setNumeroCompte(numeroCompte);
	        compteBancaire.setSoldeCompte(0.0);

	        // Simulation du comportement du repository
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.of(compteBancaire));

	        // Appel de la méthode à tester
	        CompteBancaire result = compteBancaireService.deposerMontant(numeroCompte, montant);

	        // Vérifications
	        double expectedSolde = 100.0;
	        Assertions.assertEquals(expectedSolde, result.getSoldeCompte());
	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	        verify(compteBancaireRepository, times(1)).save(compteBancaire);
	        verify(transactionRepository, times(1)).save(any(Transaction.class));
	    }

	    @Test
	    void testDeposerMontant_CompteBancaireNonTrouve() {
	        // Données de test
	        String numeroCompte = "123456";
	        double montant = 100.0;

	        // Simulation du compte bancaire non trouvé
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.empty());

	        // Vérification de l'exception
	        Assertions.assertThrows(CompteBancaireNonTrouveException.class,
	                () -> compteBancaireService.deposerMontant(numeroCompte, montant));

	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	        verify(compteBancaireRepository, never()).save(any(CompteBancaire.class));
	        verify(transactionRepository, never()).save(any(Transaction.class));
	    }
	    @Test
	    void testRetraitMontant() {
	        // Données de test
	        String numeroCompte = "123456";
	        double montant = 50.0;

	        // Compte bancaire existant
	        CompteBancaire compteBancaire = new CompteBancaire();
	        compteBancaire.setNumeroCompte(numeroCompte);
	        compteBancaire.setSoldeCompte(100.0);

	        // Simulation du comportement du repository
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.of(compteBancaire));

	        // Appel de la méthode à tester
	        CompteBancaire result = compteBancaireService.retraitMontant(numeroCompte, montant);

	        // Vérifications
	        double expectedSolde = 50.0;
	        Assertions.assertEquals(expectedSolde, result.getSoldeCompte());
	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	        verify(compteBancaireRepository, times(1)).save(compteBancaire);
	        verify(transactionRepository, times(1)).save(any(Transaction.class));
	    }

	    @Test
	    void testRetraitMontant_CompteBancaireNonTrouve() {
	        // Données de test
	        String numeroCompte = "123456";
	        double montant = 50.0;

	        // Simulation du compte bancaire non trouvé
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.empty());

	        // Vérification de l'exception
	        Assertions.assertThrows(CompteBancaireNonTrouveException.class,
	                () -> compteBancaireService.retraitMontant(numeroCompte, montant));

	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	        verify(compteBancaireRepository, never()).save(any(CompteBancaire.class));
	        verify(transactionRepository, never()).save(any(Transaction.class));
	    }
	    
	    @Test
	    void testConsulterSolde_CompteBancaireExistant() {
	        // Données de test
	        String numeroCompte = "123456";
	        double soldeAttendu = 100.0;

	        // Compte bancaire existant
	        CompteBancaire compteBancaire = new CompteBancaire();
	        compteBancaire.setNumeroCompte(numeroCompte);
	        compteBancaire.setSoldeCompte(soldeAttendu);

	        // Simulation du comportement du repository
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.of(compteBancaire));

	        // Appel de la méthode à tester
	        double soldeResultant = compteBancaireService.consulterSolde(numeroCompte);

	        // Vérification
	        Assertions.assertEquals(soldeAttendu, soldeResultant);
	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	    }

	    @Test
	    void testConsulterSolde_CompteBancaireNonTrouve() {
	        // Données de test
	        String numeroCompte = "123456";

	        // Simulation du compte bancaire non trouvé
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.empty());

	        // Vérification de l'exception
	        Assertions.assertThrows(CompteBancaireNonTrouveException.class,
	                () -> compteBancaireService.consulterSolde(numeroCompte));

	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	    }

	    @Test
	    void testCheckCompteBancaire_CompteBancaireExistant() {
	        // Données de test
	        String numeroCompte = "123456";

	        // Simulation du compte bancaire existant
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.of(new CompteBancaire()));

	        // Appel de la méthode à tester
	        boolean compteBancaireExiste = compteBancaireService.checkCompteBancaire(numeroCompte);

	        // Vérification
	        Assertions.assertTrue(compteBancaireExiste);
	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	    }
	    @Test
	    void testConsulterTransactionsPrecedentes_CompteBancaireExistant() {
	        // Données de test
	        String numeroCompte = "123456";

	        // Compte bancaire existant
	        CompteBancaire compteBancaire = new CompteBancaire();
	        compteBancaire.setNumeroCompte(numeroCompte);

	        // Liste de transactions
	        List<Transaction> transactions = new ArrayList<>();
	        transactions.add(new Transaction());
	        transactions.add(new Transaction());

	        // Simulation du comportement du repository
	        when(compteBancaireRepository.findByNumeroCompte(numeroCompte)).thenReturn(Optional.of(compteBancaire));
	        when(transactionRepository.findByCompteBancaire(compteBancaire)).thenReturn(transactions);

	        // Appel de la méthode à tester
	        List<Transaction> result = transactionServiceImpl.consulterTransactionsPrecedentes(numeroCompte);

	        // Vérifications
	        Assertions.assertEquals(transactions, result);
	        verify(compteBancaireRepository, times(1)).findByNumeroCompte(numeroCompte);
	        verify(transactionRepository, times(1)).findByCompteBancaire(compteBancaire);
	    }


}
