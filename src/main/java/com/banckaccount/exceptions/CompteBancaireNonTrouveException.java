package com.banckaccount.exceptions;

public class CompteBancaireNonTrouveException extends RuntimeException {

	public CompteBancaireNonTrouveException(String message) {
		super(message) ;
	}
}
