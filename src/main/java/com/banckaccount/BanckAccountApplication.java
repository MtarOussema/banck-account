package com.banckaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanckAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(BanckAccountApplication.class, args);
	}

}
