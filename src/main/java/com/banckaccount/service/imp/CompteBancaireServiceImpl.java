package com.banckaccount.service.imp;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banckaccount.exceptions.CompteBancaireNonTrouveException;
import com.banckaccount.model.CompteBancaire;
import com.banckaccount.model.Transaction;
import com.banckaccount.repository.CompteBancaireRepository;
import com.banckaccount.repository.TransactionRepository;
import com.banckaccount.service.CompteBancaireService;

@Service
@Transactional
public class CompteBancaireServiceImpl implements CompteBancaireService {
	@Autowired
	private CompteBancaireRepository compteBancaireRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Override
	public CompteBancaire registerCompteBancaire(CompteBancaire compteBancaire) {
		// TODO Auto-generated method stub
		return compteBancaireRepository.save(compteBancaire);
	}

	@Override
	public CompteBancaire deposerMontant(String numeroCompte, double montant) {
		Optional<CompteBancaire> compteOptional = compteBancaireRepository.findByNumeroCompte(numeroCompte);
		if (compteOptional.isPresent()) {
			CompteBancaire compte = compteOptional.get();
			double soldeActuel = compte.getSoldeCompte();
			double nouveauSolde = soldeActuel + montant;
			compte.setSoldeCompte(nouveauSolde);
			compteBancaireRepository.save(compte);
			Transaction transaction = new Transaction();
			transaction.setMontant(montant);
			transaction.setDate(new Date());
			transaction.setCompteBancaire(compte);
			transactionRepository.save(transaction);

			return compte;
		}

		else {
			throw new CompteBancaireNonTrouveException(
					"Le compte bancaire avec le numéro " + numeroCompte + " n'a pas été trouvé.");
		}

	}

	@Override
	public CompteBancaire retraitMontant(String numeroCompte, double montant) {
		// TODO Auto-generated method stub
		Optional<CompteBancaire> compteOptional = compteBancaireRepository.findByNumeroCompte(numeroCompte);
		if (compteOptional.isPresent()) {
			CompteBancaire compte = compteOptional.get();
			double soldeActuel = compte.getSoldeCompte();
			double nouveauSolde = soldeActuel - montant;
			compte.setSoldeCompte(nouveauSolde);
			compteBancaireRepository.save(compte);
			Transaction transaction = new Transaction();
			transaction.setMontant(montant);
			transaction.setDate(new Date());
			transaction.setCompteBancaire(compte);
			transactionRepository.save(transaction);
			return compte;
		} else {
			throw new CompteBancaireNonTrouveException(
					"Le compte bancaire avec le numéro " + numeroCompte + " n'a pas été trouvé.");
		}
	}

	@Override
	public double consulterSolde(String numeroCompte) {
		// TODO Auto-generated method stub
		Optional<CompteBancaire> compteOptional = compteBancaireRepository.findByNumeroCompte(numeroCompte);
		if (compteOptional.isPresent()) {
			CompteBancaire compte = compteOptional.get();
			return compte.getSoldeCompte();
		} else {
			throw new CompteBancaireNonTrouveException(
					"Le compte bancaire avec le numéro " + numeroCompte + " n'a pas été trouvé.");
		}

	}

	@Override
	public boolean checkCompteBancaire(String numeroCompte) {
		// TODO Auto-generated method stub
		Optional<CompteBancaire> compteBancaireExists = compteBancaireRepository.findByNumeroCompte(numeroCompte);
		return compteBancaireExists.isPresent();
	}

}
