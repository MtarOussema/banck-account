package com.banckaccount.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banckaccount.exceptions.CompteBancaireNonTrouveException;
import com.banckaccount.model.CompteBancaire;
import com.banckaccount.model.Transaction;
import com.banckaccount.repository.CompteBancaireRepository;
import com.banckaccount.repository.TransactionRepository;
import com.banckaccount.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private CompteBancaireRepository compteBancaireRepository;

	@Override
	public List<Transaction> consulterTransactionsPrecedentes(String numeroCompte) {
		// TODO Auto-generated method stub
		Optional<CompteBancaire> compteOptional = compteBancaireRepository.findByNumeroCompte(numeroCompte);
		if (compteOptional.isPresent()) {
			CompteBancaire compteBancaire = compteOptional.get();
			return transactionRepository.findByCompteBancaire(compteBancaire);
		} else {
			throw new CompteBancaireNonTrouveException(
					"Le compte bancaire avec le numéro " + numeroCompte + " n'a pas été trouvé.");
		}
	}

}
