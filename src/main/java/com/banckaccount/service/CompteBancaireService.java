package com.banckaccount.service;

import com.banckaccount.model.CompteBancaire;

public interface CompteBancaireService {

	CompteBancaire registerCompteBancaire(CompteBancaire compteBancaire);

	CompteBancaire deposerMontant(String numeroCompte, double montant);

	CompteBancaire retraitMontant(String numeroCompte, double montant);

	double consulterSolde(String numeroCompte);

	boolean checkCompteBancaire(String numeroCompte);

}
