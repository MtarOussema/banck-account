package com.banckaccount.service;

import java.util.List;

import com.banckaccount.model.Transaction;

public interface TransactionService {

	List<Transaction> consulterTransactionsPrecedentes(String numeroCompte);
}
