package com.banckaccount.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banckaccount.model.CompteBancaire;
import com.banckaccount.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	List<Transaction> findByCompteBancaire(CompteBancaire compteBancaire);

}
