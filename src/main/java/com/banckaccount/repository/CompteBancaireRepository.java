package com.banckaccount.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banckaccount.model.CompteBancaire;

@Repository
public interface CompteBancaireRepository extends JpaRepository<CompteBancaire, Long> {

	Optional<CompteBancaire> findByNumeroCompte(String numeroCompte);

}
