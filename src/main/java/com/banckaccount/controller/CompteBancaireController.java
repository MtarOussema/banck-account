package com.banckaccount.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banckaccount.model.CompteBancaire;
import com.banckaccount.service.CompteBancaireService;

@RestController
@RequestMapping("/comptes")
public class CompteBancaireController {

	@Autowired
	private CompteBancaireService compteBancaireService;

	@PostMapping("/register")
	public ResponseEntity<String> registerCompteBancaire(@RequestBody CompteBancaire compteBancaire) {
		boolean isExistingAccount = compteBancaireService.checkCompteBancaire(compteBancaire.getNumeroCompte());

		if (isExistingAccount) {
			String message = "Le compte existe déjà.";
			return ResponseEntity.status(HttpStatus.CONFLICT).body(message);
		}
		System.out.println("test ");
		compteBancaireService.registerCompteBancaire(compteBancaire);
		String message = "Compte a été  enregistré.";
		return ResponseEntity.ok(message);
	}

	@PostMapping("/{numeroCompte}/deposer")
	public ResponseEntity<CompteBancaire> deposerMontant(@PathVariable String numeroCompte,
			@RequestParam double montant) {

		CompteBancaire compteBancaire = compteBancaireService.deposerMontant(numeroCompte, montant);
		return ResponseEntity.ok(compteBancaire);
	}

	@PostMapping("/{numeroCompte}/retirer")
	public ResponseEntity<CompteBancaire> retraitMontant(@PathVariable String numeroCompte,
			@RequestParam double montant) {
		CompteBancaire compteBancaire = compteBancaireService.retraitMontant(numeroCompte, montant);
		return ResponseEntity.ok(compteBancaire);
	}

	@GetMapping("/{numeroCompte}/solde")
	public ResponseEntity<Double> consulterSolde(@PathVariable String numeroCompte) {
		double solde = compteBancaireService.consulterSolde(numeroCompte);
		return ResponseEntity.ok(solde);
	}
}