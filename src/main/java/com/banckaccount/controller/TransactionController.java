package com.banckaccount.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banckaccount.model.Transaction;
import com.banckaccount.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@GetMapping("/{numeroCompte}/transactions")
	public ResponseEntity<List<Transaction>> consulterTransactions(@PathVariable String numeroCompte) {
		List<Transaction> transactions = transactionService.consulterTransactionsPrecedentes(numeroCompte);
		return ResponseEntity.ok(transactions);
	}
}
